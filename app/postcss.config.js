module.exports =  {
  syntax: 'postcss-scss',
  plugins: [
    require('postcss-smart-import')({
      path: [
        'templates'
      ]
    }),
    require('postcss-cssnext')
  ]
};
