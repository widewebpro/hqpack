import './index.scss';
import $ from 'jquery';
import 'aos/dist/aos';
import 'aos/dist/aos.css';

import AOS from 'aos';

// import 'mdbootstrap';
// import 'mdbootstrap/js/mdb.min.js.map';
// import 'mdbootstrap/js/mdb.';
// import { Input } from 'mdb-ui-kit';
// import { Input as CustomInput } from 'mdb-ui-kit';

AOS.init();

class LazyLoadNew {
    constructor(selector) {
        this.selectorForLazyLoad = selector
        this.pictures = $(`.${selector}`)
    }
    imgUI({
              imgClass, src, altText,
              urlMobileWebP, urlTabletWebP, urlDesktopWebP,
              urlMobile, urlTablet, urlDesktop
          }) {
        return `
    <source srcset="${urlMobileWebP}" media="screen and (max-device-width: 640px)" type="image/webp">
    <source srcset="${urlTabletWebP}" media="(min-device-width: 641px) and (max-device-width: 1025px)" type="image/webp">
    <source srcset="${urlDesktopWebP}" media="(min-device-width: 1026px)" type="image/webp">
    <img src="${src}" alt="${altText}"
      class="${imgClass}"
      srcset="${urlMobile} 1920w,
              ${urlTablet} 1440w,
              ${urlDesktop} 940w"
      sizes="screen and (max-device-width: 640px) 280px,
            (min-device-width: 641px) and (max-device-width: 1025px) 440px,
            (min-device-width: 1026px) 1440px"
    >
    `
    }
    picturesInit() {
        this.pictures = $(`.${ this.selectorForLazyLoad }`)
    }
    lazyLoading() {
        const scrollTop = $(window).scrollTop()
        this.picturesInit()
        this.pictures.each((ind, picture) => {
            if (
                (scrollTop > ($(picture).offset().top - ($(window).height() + 100)) ||
                    $(picture).offset().top < ($(window).height() + 100)) &&
                $(picture).hasClass(this.selectorForLazyLoad)
            ) {
                const imgParams = {
                    src: $(picture).data('src'),
                    imgClass: $(picture).data('imgClass'),
                    altText: $(picture).data('altText'),
                    urlMobileWebP: $(picture).data('mobileWebp'),
                    urlTabletWebP: $(picture).data('tabletWebp'),
                    urlDesktopWebP: $(picture).data('desktopWebp'),
                    urlMobile: $(picture).data('urlMobile'),
                    urlTablet: $(picture).data('urlTablet'),
                    urlDesktop: $(picture).data('urlDesktop')
                }
                $(picture).append(this.imgUI(imgParams))
                if ($(picture).find('img')) {
                    $(picture).removeClass(this.selectorForLazyLoad)
                }
            }
        })
    }
}

$(() => {
    if ($('.lazy-load-new').length) {
        const LazyNew = new LazyLoadNew('lazy-load-new')
        LazyNew.lazyLoading()
        $(window).on('scroll', () => {
            if ($('.lazy-load-new').length) LazyNew.lazyLoading()
        })
    }
})

$(document).ready(function(){
    $(".search-btn--js").click(function(){
        $(".header__wrap").slideToggle();
    });
    $(".search-btn--js").click(function () {
        $(this).toggleClass("active");
    });
    $('.header__search-input--js input').keyup(function(){
        if ($('.header__search-input--js input').val()) {
            $('.header__search-button--js').addClass('input-red');
        } else {
            $('.header__search-button--js').removeClass('input-red');
        }
    });

    // $(window).on("load",function() {
    //     function fade() {
    //         var animation_height = $(window).innerHeight() * 0.25;
    //         var ratio = Math.round( (1 / animation_height) * 10000 ) / 10000;
    //
    //         $('.fade').each(function() {
    //
    //             var objectTop = $(this).offset().top;
    //             var windowBottom = $(window).scrollTop() + $(window).innerHeight();
    //
    //             if ( objectTop < windowBottom ) {
    //                 if ( objectTop < windowBottom - animation_height ) {
    //                     $(this).css( {
    //                         transition: 'opacity 0.1s linear',
    //                         opacity: 1
    //                     } );
    //
    //                 } else {
    //                     $(this).css( {
    //                         transition: 'opacity 0.25s linear',
    //                         opacity: (windowBottom - objectTop) * ratio
    //                     } );
    //                 }
    //             } else {
    //                 $(this).css( 'opacity', 0 );
    //             }
    //         });
    //     }
    //     $('.fade').css( 'opacity', 0 );
    //     fade();
    //     $(window).scroll(function() {fade();});
    // });
});
