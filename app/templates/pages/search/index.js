import './index.scss'
import $ from "jquery";

$(document).ready(function(){
    $('.result__search-input--js input').keyup(function(){
        if ($('.result__search-input--js input').val()) {
            $('.result__search-button--js').addClass('input-red');
        } else {
            $('.result__search-button--js').removeClass('input-red');
        }
    });
});