import './index.scss';
import $ from 'jquery';
import 'slick-carousel';

$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    // arrows: true,
    appendArrows: $('.slider-nav-arrow'),
    prevArrow: '<button id="prev" type="button" class="btn btn-juliet"><span></span> Prev</button>',
    nextArrow: '<button id="next" type="button" class="btn btn-juliet">Next <span></span></button>',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
            }
        }
    ],
});