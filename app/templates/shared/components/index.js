import './banner';
import './banner-img';
import './solutions';
import './preview-content';
import './get-started';
import './contact-banner';
import './vacancies';
import './text-content';
import './divider';
import './about-office';
import './form';
import './slider';
import './bread-crumbs';
import './map'