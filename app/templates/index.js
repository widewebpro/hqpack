import './layout';
import '../web/assets/global-styles';
import './shared';
import './pages';
import './pages/about-hq-pack';
import '_bootstrap.scss';
import 'jquery';
import 'bootstrap/dist/js/bootstrap.js'
import '@fortawesome/fontawesome-free/js/brands.js'
import '@fortawesome/fontawesome-free/js/solid.js'
import '@fortawesome/fontawesome-free/js/fontawesome.js'


// import 'bootstrap-icons/font/bootstrap-icons.css'

// import '_bootstrap-variables.scss';
// import 'bootstrap/scss/bootstrap.scss';

// import 'bootstrap/dist/css/bootstrap.min.css'